import {Component, OnInit} from '@angular/core';
import {ProductDTO} from "../model/productDTO";
import {ProductsService} from "../service/products.service";

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  products: ProductDTO[] = [];

  constructor(private productsService: ProductsService) {
    this.productsService = productsService;
  }

  ngOnInit(): void {
    this.productsService.findAll().subscribe(successfulResponse => {
      console.log('Successful response received ' + successfulResponse);
      // @ts-ignore
      this.products = successfulResponse;
    })
  }

}
