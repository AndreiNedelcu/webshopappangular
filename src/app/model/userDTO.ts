export class UserDTO {
  id: String;
  userName: String;

  constructor(id: String, name: String) {
    this.id = id;
    this.userName = name;
  }
}
