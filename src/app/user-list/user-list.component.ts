import { Component, OnInit } from '@angular/core';
import {UserDTO} from "../model/userDTO";
import {UsersService} from "../service/users.service";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  users: UserDTO[] = [];

  constructor(private usersService: UsersService) {
    this.usersService = usersService;
  }


  ngOnInit(): void {
    this.usersService.findAll().subscribe(successfulResponse =>{
      console.log('Successful response received ' + successfulResponse);
      //@ts-ignore
      this.users = successfulResponse;
    })
  }

}
